const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
    console.log(map(1,0,2,0,50));
  console.log(`Example app listening at http://localhost:${port}`)
})

/**
 * This function returns a value in a scope in to a different scope
 * For example: 0.5 is a value that is expected between 0 and 1 but we need the same value
 * in the scope of 10 to 20 thinking, this will lead to 15 in this scope.
 * @param {*} val value to convert
 * @param {*} xstart lowest value possible in the first scope
 * @param {*} xend highest value possible in the first scope
 * @param {*} ystart lowest value possible in the second scope
 * @param {*} yend highest value possible in the first scope
 */

function map(val,xstart,xend,ystart,yend){
    return ystart + (yend-ystart) * ((val-xstart)/(xend-xstart))
}